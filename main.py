import importlib
import os

from flask import Flask, Response
import tomllib

app = Flask(__name__)

with open(os.environ['RSS_REWRITER_CONFIG'], 'rb') as f:
    configs = tomllib.load(f)

@app.route('/<output_name>.xml')
def run_script(output_name):
    config = dict(configs[output_name]) # Make a copy, which will be modified below
    script = config.pop('script')
    script_module = importlib.import_module(script.replace('-', '_'))
    feed = script_module.run(app, **config)
    return Response(feed, mimetype='text/xml')
