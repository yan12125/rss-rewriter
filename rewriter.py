import html
import urllib.error
import urllib.request

from lxml import etree as ElementTree

# References:
# RSS 2.0 https://validator.w3.org/feed/docs/rss2.html
# Atom https://datatracker.ietf.org/doc/html/rfc4287
NS = {
    'atom': 'http://www.w3.org/2005/Atom',
}
ENTRY_PATH = {
    'rss': './channel/item',
    'feed': './atom:entry',
}
ID_TAG = {
    'rss': 'guid',
    'feed': 'atom:id',
}
CONTENT_TAG = {
    'rss': 'description',
    'feed': 'atom:content',
}

def run(app, rss_url: str, **kwargs):
    guid_field = kwargs.get('guid_field')

    headers = {}
    if kwargs.get('user_agent'):
        headers['User-Agent'] = kwargs['user_agent']
    req = urllib.request.Request(rss_url, headers=headers)

    N_TRIALS = 2
    for idx in range(N_TRIALS):
        try:
            with urllib.request.urlopen(req) as resp:
                content = resp.read()
                break
        except urllib.error.URLError as e:
            app.logger.warning('Got error: %s', e)
            if idx == N_TRIALS - 1 or not isinstance(e.reason, ConnectionResetError):
                raise

    rss = ElementTree.fromstring(content)
    feed_type = ElementTree.QName(rss).localname

    for item in rss.findall(ENTRY_PATH[feed_type], NS):
        updated = item.find('atom:updated', NS)
        if updated is not None:
            pub_date = ElementTree.Element('pubDate')
            pub_date.text = updated.text
            item.append(pub_date)

        if guid_field:
            guid = item.find('./' + ID_TAG[feed_type])
            new_guid = item.find(f'./{guid_field}').text
            if guid is not None:
                if guid.get('isPermaLink') != 'true':
                    guid.text = new_guid
            else:
                guid = ElementTree.Element(ID_TAG[feed_type])
                guid.text = new_guid
                item.append(guid)

        content_node = item.find('./' + CONTENT_TAG[feed_type], NS)
        content = content_node.text or ''
        if kwargs.get('text2html', False):
            content = html.escape(content)
            content = content.replace('\n', '<br />')
        else:
            content = html.unescape(content)
        content_node.text = content

    return ElementTree.tostring(rss, encoding='unicode')
