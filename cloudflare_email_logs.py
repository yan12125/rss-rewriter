import datetime
import json
import textwrap
import urllib.error
import urllib.request
from typing import Any

import jinja2
from feedgenerator.django.utils.feedgenerator import Atom1Feed

def request(config: dict[str, str], now: datetime.datetime, one_day_ago: datetime.datetime) -> dict[str, Any]:
    # Uses an unofficial API observed from the dashboard. See also
    # https://community.cloudflare.com/t/an-activity-log-of-email-routing-using-an-api/383500
    data = {
        'query': textwrap.dedent('''{
          viewer {
            zones(filter: {zoneTag: $zoneTag}) {
              emailRoutingAdaptive(limit: 1000, filter: $filter, orderBy: [datetime_DESC, from_ASC, to_ASC, status_ASC]) {
                datetime
                sessionId
                subject
                from
                to
                status
                spf
                dkim
                dmarc
                errorDetail
              }
            }
          }
        }'''),
        'variables': {
            'filter': {
                'datetime_geq': one_day_ago.isoformat(),
                'datetime_leq': now.isoformat(),
            },
            'zoneTag': config['cloudflare_zone_id'],
        },
    }
    # Needs Zone.Analytics:Read
    req = urllib.request.Request(
        url='https://api.cloudflare.com/client/v4/graphql',
        data=json.dumps(data).encode('ascii'),
        headers={
            'Content-Typ': 'application/json',
            # email is necessary
            # https://developers.cloudflare.com/analytics/graphql-api/getting-started/authentication/graphql-client-headers/
            'X-AUTH-EMAIL': config['cloudflare_email'],
            'Authorization': f'Bearer {config["cloudflare_token"]}'
        }
    )
    resp = urllib.request.urlopen(req)
    return json.loads(resp.read())

def gen_feed(data: dict[str, Any], now: datetime.datetime, one_day_ago: datetime.datetime) -> str:
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'), autoescape=True)
    template = env.get_template('cloudflare_email_logs.html.j2')
    content = template.render(items=data['data']['viewer']['zones'][0]['emailRoutingAdaptive'])

    feed = Atom1Feed(
        title='Cloudflare email routing report',
        link='https://cloudflare.com/',
        description='Cloudflare email routing report',
    )
    if content.strip():
        feed.add_item(
            title=f'Report for mails from {one_day_ago} to {now}',
            link='https://cloudflare.com/',
            description=content,
            pubdate=now,
        )
    return feed.writeString('utf-8')

def run(app, **kwargs) -> str:
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    one_day_ago = now - datetime.timedelta(days=1)

    data = request(kwargs, now, one_day_ago)
    return gen_feed(data, now, one_day_ago)
